package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class OrderDrinkTest {

    @Test
    public void testOrderDrink() {
        OrderDrink orderDrink = OrderDrink.getInstance();
        orderDrink.setDrink("DrinkTest");
        assertEquals("DrinkTest", orderDrink.getDrink());
        assertEquals("DrinkTest", orderDrink.toString());
    }
}