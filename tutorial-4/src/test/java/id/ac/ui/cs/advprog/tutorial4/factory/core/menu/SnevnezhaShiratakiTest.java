package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;

public class SnevnezhaShiratakiTest {
    private Menu menu;

    @BeforeEach
    public void setUp() {
        menu = new SnevnezhaShirataki("SnevnezhaTest");
    }

    @Test
    public void testSnevnezhaGetName() throws Exception {
        assertEquals("SnevnezhaTest", menu.getName());
    }

    @Test
    public void testSnevnezhaGetNoodle() throws Exception {
        assertTrue(menu.getNoodle() instanceof Shirataki);
    }

    @Test
    public void testSnevnezhaGetMeat() throws Exception {
        assertTrue(menu.getMeat() instanceof Fish);
    }

    @Test
    public void testSnevnezhaGetTopping() throws Exception {
        assertTrue(menu.getTopping() instanceof Flower);
    }

    @Test
    public void testSnevnezhaetFlavor() throws Exception {
        assertTrue(menu.getFlavor() instanceof Umami);
    }
}