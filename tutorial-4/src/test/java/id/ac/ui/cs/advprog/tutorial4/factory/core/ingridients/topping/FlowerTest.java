package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class FlowerTest {

    @Test
    public void testFlowerGetDescription() throws Exception {
        Flower flower = new Flower();
        assertEquals("Adding Xinqin Flower Topping...", flower.getDescription());
    }
}
