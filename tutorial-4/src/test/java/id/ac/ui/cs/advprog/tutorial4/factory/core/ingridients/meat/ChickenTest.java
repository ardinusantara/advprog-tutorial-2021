package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ChickenTest {

    @Test
    public void testChickenGetDescription() throws Exception {
        Chicken chicken = new Chicken();
        assertEquals("Adding Wintervale Chicken Meat...", chicken.getDescription());
    }
}