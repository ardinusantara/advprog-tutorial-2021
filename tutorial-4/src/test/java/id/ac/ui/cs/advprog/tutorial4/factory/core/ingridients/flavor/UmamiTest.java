package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class UmamiTest {

    @Test
    public void testUmamiGetDescription() throws Exception {
        Umami umami = new Umami();
        assertEquals("Adding WanPlus Specialty MSG flavoring...", umami.getDescription());
    }
}