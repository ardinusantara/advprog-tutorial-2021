package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class FishTest {

    @Test
    public void testFishGetDescription() throws Exception {
        Fish fish = new Fish();
        assertEquals("Adding Zhangyun Salmon Fish Meat...", fish.getDescription());
    }
}