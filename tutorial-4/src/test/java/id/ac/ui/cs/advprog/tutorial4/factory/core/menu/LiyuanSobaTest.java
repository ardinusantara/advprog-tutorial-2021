package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;

public class LiyuanSobaTest {
    private Menu menu;

    @BeforeEach
    public void setUp() {
        menu = new LiyuanSoba("LiyuanTest");
    }

    @Test
    public void testLiyuanGetName() throws Exception {
        assertEquals("LiyuanTest", menu.getName());
    }

    @Test
    public void testLiyuanGetNoodle() throws Exception {
        assertTrue(menu.getNoodle() instanceof Soba);
    }

    @Test
    public void testLiyuanGetMeat() throws Exception {
        assertTrue(menu.getMeat() instanceof Beef);
    }

    @Test
    public void testLiyuanGetTopping() throws Exception {
        assertTrue(menu.getTopping() instanceof Mushroom);
    }

    @Test
    public void testLiyuanGetFlavor() throws Exception {
        assertTrue(menu.getFlavor() instanceof Sweet);
    }
}