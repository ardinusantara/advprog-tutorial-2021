package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class MushroomTest {

    @Test
    public void testMushroomGetDescription() throws Exception {
        Mushroom mushroom = new Mushroom();
        assertEquals("Adding Shiitake Mushroom Topping...", mushroom.getDescription());
    }
}
