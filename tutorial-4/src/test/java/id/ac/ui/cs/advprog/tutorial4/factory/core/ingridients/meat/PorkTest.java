package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class PorkTest {

    @Test
    public void testPorkGetDescription() throws Exception {
        Pork pork = new Pork();
        assertEquals("Adding Tian Xu Pork Meat...", pork.getDescription());
    }
}