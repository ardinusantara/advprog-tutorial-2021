package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;

import java.util.List;


public class MenuServiceImplTest {
    private MenuServiceImpl menuServiceImpl;
    private MenuServiceImpl menuServiceImpl2;

    @BeforeEach
    public void setUp() {
        menuServiceImpl = new MenuServiceImpl();
        menuServiceImpl2 = new MenuServiceImpl(new MenuRepository());
    }

    @Test
    public void testMenuServiceImplCreateMenu() throws Exception {
        menuServiceImpl.createMenu("InuzumaTest", "InuzumaRamen");
        menuServiceImpl.createMenu("LiyuanTest", "LiyuanSoba");
        menuServiceImpl.createMenu("MondoTest", "MondoUdon");
        menuServiceImpl.createMenu("SnevnezhaTest", "SnevnezhaShirataki");
        assertEquals(8, menuServiceImpl.getMenus().size());
    }

    @Test
    public void testMenuServiceImplGetMenus() {
        menuServiceImpl.createMenu("InuzumaTest", "InuzumaRamen");
        assertTrue(menuServiceImpl.getMenus() instanceof List);
    }
}