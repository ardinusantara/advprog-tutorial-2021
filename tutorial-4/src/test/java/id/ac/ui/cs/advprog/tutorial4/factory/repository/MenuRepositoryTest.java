package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;

import java.util.List;

public class MenuRepositoryTest {
    private MenuRepository menuRepository;

    @BeforeEach
    public void setUp() {
        menuRepository = new MenuRepository();
    }

    @Test
    public void testMenuRepositoryGetMenus() throws Exception {
        assertTrue(menuRepository.getMenus() instanceof List);
    }

    @Test
    public void testMenuRepositoryAdd() throws Exception {
        menuRepository.add(new SnevnezhaShirataki("SnevnezhaTest"));
        assertEquals(1, menuRepository.getMenus().size());
    }
}