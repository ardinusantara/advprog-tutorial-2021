package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class OrderServiceImplTest {
    private OrderServiceImpl orderServiceImpl;

    @BeforeEach
    public void setUp() {
        orderServiceImpl = new OrderServiceImpl();
    }

    @Test
    public void testOrderServiceImplOrderADrink() {
        orderServiceImpl.orderADrink("DrinkTest");
        assertEquals("DrinkTest", orderServiceImpl.getDrink().getDrink());
    }

    @Test
    public void testOrderServiceImplGetDrink() {
        assertNotNull(orderServiceImpl.getDrink());
    }

    @Test
    public void testOrderServiceImplOrderAFood() {
        orderServiceImpl.orderAFood("FoodTest");
        assertEquals("FoodTest", orderServiceImpl.getFood().getFood());
    }

    @Test
    public void testOrderServiceImplGetFood() {
        assertNotNull(orderServiceImpl.getFood());
    }
}
