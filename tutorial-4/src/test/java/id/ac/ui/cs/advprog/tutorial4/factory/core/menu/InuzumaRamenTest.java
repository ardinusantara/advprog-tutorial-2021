package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;

public class InuzumaRamenTest {
    private Menu menu;

    @BeforeEach
    public void setUp() {
        menu = new InuzumaRamen("InuzumaTest");
    }

    @Test
    public void testInuzumaGetName() throws Exception {
        assertEquals("InuzumaTest", menu.getName());
    }

    @Test
    public void testInuzumaGetNoodle() throws Exception {
        assertNotNull(menu.getNoodle());
        assertTrue(menu.getNoodle() instanceof Ramen);
    }

    @Test
    public void testInuzumaGetMeat() throws Exception {
        assertNotNull(menu.getMeat());
        assertTrue(menu.getMeat() instanceof Pork);
    }

    @Test
    public void testInuzumaGetTopping() throws Exception {
        assertNotNull(menu.getTopping());
        assertTrue(menu.getTopping() instanceof BoiledEgg);
    }

    @Test
    public void testInuzumaGetFlavor() throws Exception {
        assertNotNull(menu.getFlavor());
        assertTrue(menu.getFlavor() instanceof Spicy);
    }
}