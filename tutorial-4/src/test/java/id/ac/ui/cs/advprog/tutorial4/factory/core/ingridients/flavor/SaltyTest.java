package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SaltyTest {

    @Test
    public void testSaltyGetDescription() throws Exception {
        Salty salty = new Salty();
        assertEquals("Adding a pinch of salt...", salty.getDescription());
    }
}