package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;

public class MondoUdonTest {
    private Menu menu;

    @BeforeEach
    public void setUp() {
        menu = new MondoUdon("MondoTest");
    }

    @Test
    public void testMondoGetName() throws Exception {
        assertEquals("MondoTest", menu.getName());
    }

    @Test
    public void testMondoGetNoodle() throws Exception {
        assertTrue(menu.getNoodle() instanceof Udon);
    }

    @Test
    public void testMondoGetMeat() throws Exception {
        assertTrue(menu.getMeat() instanceof Chicken);
    }

    @Test
    public void testMondoGetTopping() throws Exception {
        assertTrue(menu.getTopping() instanceof Cheese);
    }

    @Test
    public void testMondoGetFlavor() throws Exception {
        assertTrue(menu.getFlavor() instanceof Salty);
    }
}