package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class UdonTest {

    @Test
    public void testUdonGetDescription() throws Exception {
        Udon udon = new Udon();
        assertEquals("Adding Mondo Udon Noodles...", udon.getDescription());
    }
}