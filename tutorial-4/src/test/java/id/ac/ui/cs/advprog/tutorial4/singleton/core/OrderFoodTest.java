package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class OrderFoodTest {

    @Test
    public void testOrderFood() {
        OrderFood orderFood = OrderFood.getInstance();
        orderFood.setFood("FoodTest");
        assertEquals("FoodTest", orderFood.getFood());
        assertEquals("FoodTest", orderFood.toString());
    }
}