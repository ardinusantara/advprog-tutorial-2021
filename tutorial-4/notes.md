## Perbedaan Lazy dan Eager Instantiation
- Lazy Instantiation hanya akan membuat instance ketika objek tersebut dibutuhkan 
  sedangkan Eager Instantiation akan langsung membuat instance ketika program dijalankan.
- Lazy Instantiation harus mengecek apakah instance masih null terlebih dahulu sedangkan
  Eager Instantiation tidak perlu.
  
## Keuntungan dan Kelebihan
### Lazy Instantiation
#### Kelebihan:
1. Objek dari class hanya diinstansiasi pada saat dibutuhkan
   sehingga dapat menghemat sumber daya.
2. Bisa dilakukan Exception handling jika diinginkan

#### Kekurangan:
1. Terdapat isu multithreading sehingga harus diatasi 
   dengan synchronization yang cukup memakan banyak sumber 
   daya.
2. Harus selalu mengecek kondisi null saat instansiasi

### Eager Instantiation
#### Kelebihan:
1. Thread-safe (tidak ada isu multithreading)
2. Mudah diimplementasi
3. Siap untuk langsung diakses

#### Kekurangan:
1. Memakan banyak sumber daya
2. Tidak bisa dilakukan Exeception handling