package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ElementalTransformationTest {
    private Class<?> elementalClass;

    @BeforeEach
    public void setup() throws Exception {
        elementalClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.ElementalTransformation");
    }

    @Test
    public void testElementalHasEncodeMethod() throws Exception {
        Method translate = elementalClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testElementalEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "LTYbkT3TgW3B3pXgm3mh3T3UeTVdlfbma3mh3YhkZX3hnk3lphkW";

        Spell result = new ElementalTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testElementalHasDecodeMethod() throws Exception {
        Method translate = elementalClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testElementalDecodesCorrectly() throws Exception {
        String text = "LTYbkT3TgW3B3pXgm3mh3T3UeTVdlfbma3mh3YhkZX3hnk3lphkW";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new ElementalTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }
}
