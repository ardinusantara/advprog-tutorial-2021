package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;

/**
 * Kelas ini melakukan caesar cipher dengan shift amount 7
 */
public class ElementalTransformation {
    private int key;

    public ElementalTransformation(){
        this.key = 7;
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int selector = encode ? -1 : 1;
        char[] res = new char[text.length()];
        for(int i = 0; i < text.length(); i++){
            int newIdx = (codex.getIndex(text.charAt(i)) + key * selector) % codex.getCharSize();
            if (newIdx < 0) {
                newIdx += codex.getCharSize();
            }
            res[i] = codex.getChar(newIdx);
        }

        return new Spell(new String(res), codex);
    }
}
