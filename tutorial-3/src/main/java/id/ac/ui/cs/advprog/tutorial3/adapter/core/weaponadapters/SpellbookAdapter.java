package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean status;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.status = true;
    }

    @Override
    public String normalAttack() {
        status = true;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (status) {
            status = false;
            return spellbook.largeSpell();
        } else {
            return "Insufficient mana!";
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
