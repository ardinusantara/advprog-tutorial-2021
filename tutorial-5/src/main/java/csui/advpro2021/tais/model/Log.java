package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
public class Log {
    @Id
    @Column(name = "id_log", updatable = false, nullable = false, columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idLog;

    @Column(name = "start_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;

    @Column(name = "end_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;

    @Column(name = "deskripsi")
    private String deskripsi;

    @ManyToOne
    @JoinColumn(name = "mahasiswa", nullable = false)
    private Mahasiswa mahasiswa;

    public Log(Date startTime, Date endTime, String deskripsi) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.deskripsi = deskripsi;
    }

    @JsonGetter("mahasiswa")
    public String getJsonMataKuliah() {
        if (mahasiswa == null) {
            return "";
        }
        return mahasiswa.getNpm();
    }
}
