package csui.advpro2021.tais.model;

import lombok.Data;

@Data
public class LogSummary {
    String bulan;
    int jamKerja;
    long pembayaran;

    public LogSummary(String bulan, int jamKerja, long pembayaran) {
        this.bulan = bulan;
        this.jamKerja = jamKerja;
        this.pembayaran = pembayaran;
    }

    public LogSummary(String bulan) {
        this(bulan, 0, 0);
    }
}
