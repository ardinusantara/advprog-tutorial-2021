package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogRepository logRepository;

    @Override
    public Log createLog(Mahasiswa mahasiswa, Log log) {
        log.setMahasiswa(mahasiswa);
        logRepository.save(log);
        return log;
    }

    @Override
    public Iterable<Log> getListLog() {
        return  logRepository.findAll();
    }

    @Override
    public Log getLogByIdLog(Integer idLog) {
        return logRepository.findByIdLog(idLog);
    }

    @Override
    public List<Log> getListLogByMahasiswa(Mahasiswa mahasiswa) {
        return logRepository.findListLogByMahasiswa(mahasiswa);
    }

    @Override
    public Collection<LogSummary> getLogSummary(Mahasiswa mahasiswa) {
        Calendar calendar = Calendar.getInstance();

        List<Log> logList = getListLogByMahasiswa(mahasiswa);
        HashMap<Integer, LogSummary> res = new HashMap<>();
        for (Log log : logList) {
            Date start = log.getStartTime();
            Date end = log.getEndTime();
            int time = (int) ((end.getTime() - start.getTime()) / (60 * 60 * 1000));

            calendar.setTime(start);
            int month = calendar.get(Calendar.MONTH);
            String timeDisplay = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());

            LogSummary logSummary = res.getOrDefault(month, new LogSummary(timeDisplay));
            logSummary.setJamKerja(logSummary.getJamKerja() + time);
            logSummary.setPembayaran(logSummary.getJamKerja() * 350L);

            res.put(month, logSummary);
        }

        return res.values();

    }

    @Override
    public LogSummary getLogSummaryByMonth(Mahasiswa mahasiswa, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(0, month, 1);

        List<Log> logList = getListLogByMahasiswa(mahasiswa);
        String timeDisplay = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        LogSummary logSummary = new LogSummary(timeDisplay);
        for (Log log : logList) {
            calendar.setTime(log.getStartTime());
            if (calendar.get(Calendar.MONTH) == month) {
                Date start = log.getStartTime();
                Date end = log.getEndTime();
                int time = (int) ((end.getTime() - start.getTime()) / (60 * 60 * 1000));

                logSummary.setJamKerja(logSummary.getJamKerja() + time);
            }
        }
        logSummary.setPembayaran(logSummary.getJamKerja() * 350L);

        return logSummary;

    }

    @Override
    public Log updateLog(Integer idLog, Log log) {
        log.setIdLog(idLog);
        logRepository.save(log);
        return log;
    }

    @Override
    public void deleteLogByIdLog(Integer idLog) {
        logRepository.deleteById(idLog);
    }
}
