package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public interface LogService {
    Log createLog(Mahasiswa mahasiswa, Log log);

    Iterable<Log> getListLog();

    Log getLogByIdLog(Integer idLog);

    List<Log> getListLogByMahasiswa(Mahasiswa mahasiswa);

    Collection<LogSummary> getLogSummary(Mahasiswa mahasiswa);

    LogSummary getLogSummaryByMonth(Mahasiswa mahasiswa, int month);

    Log updateLog(Integer idLog, Log log);

    void deleteLogByIdLog(Integer idLog);
}
