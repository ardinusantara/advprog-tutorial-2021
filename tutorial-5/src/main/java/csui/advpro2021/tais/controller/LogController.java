package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.LogService;
import csui.advpro2021.tais.service.MahasiswaService;
import csui.advpro2021.tais.service.MataKuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/asisten-dosen")
public class LogController {
    @Autowired
    private LogService logService;

    @Autowired
    private MahasiswaService mahasiswaService;

    @Autowired
    private MataKuliahService mataKuliahService;

    @PostMapping(path = "/daftar/{npm}/{kodeMatkul}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity daftarAsisten(@PathVariable(value = "npm") String npm,
                                        @PathVariable(value = "kodeMatkul") String kodeMatkul) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);

        if (mahasiswa.getMatkulDiAsdosi() != null) {
            return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
        }
        MataKuliah mataKuliah = mataKuliahService.getMataKuliah(kodeMatkul);

        return ResponseEntity.ok(mahasiswaService.setJadiAsdos(mahasiswa, mataKuliah));
    }

    @GetMapping(path = "/logList", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Log>> getListLog() {
        Iterable<Log> log = logService.getListLog();
        return ResponseEntity.ok(logService.getListLog());
    }

    @GetMapping(path = "/log/{idLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLogById(@PathVariable(value = "idLog") Integer idLog) {
        Log log = logService.getLogByIdLog(idLog);
        if (log == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(logService.getLogByIdLog(idLog));
    }

    @PutMapping(path = "/log/update/{idLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@PathVariable(value = "idLog") Integer idLog, @RequestBody Log log) {
        Log tempLog = logService.getLogByIdLog(idLog);
        if (tempLog == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(logService.updateLog(idLog, log));
    }

    @DeleteMapping(path = "/log/delete/{idLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity deleteLog(@PathVariable(value = "idLog") Integer idLog) {
        Log log = logService.getLogByIdLog(idLog);
        if (log == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        logService.deleteLogByIdLog(idLog);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/summary/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLogSummary(@PathVariable(value = "npm") String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null || mahasiswa.getMatkulDiAsdosi() == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(logService.getLogSummary(mahasiswa));
    }

    @GetMapping(path = "/summaryMonth/{npm}/{month}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLogSummaryByMonth(@PathVariable(value = "npm") String npm,
                                               @PathVariable(value = "month") Integer month) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null || mahasiswa.getMatkulDiAsdosi() == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(logService.getLogSummaryByMonth(mahasiswa, month));
    }

    @PostMapping(path = "/log/create/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity addLog(@PathVariable(value = "npm") String npm, @RequestBody Log log) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null || mahasiswa.getMatkulDiAsdosi() == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(logService.createLog(mahasiswa, log));
    }

    @GetMapping(path = "/log/{npm}/logList", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getMahasiswaLogList(@PathVariable(value = "npm") String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null || mahasiswa.getMatkulDiAsdosi() == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(logService.getListLogByMahasiswa(mahasiswa));
    }
}
