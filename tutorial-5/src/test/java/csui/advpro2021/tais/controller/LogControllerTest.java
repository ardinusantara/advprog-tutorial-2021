package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    @MockBean
    private MataKuliahServiceImpl mataKuliahService;

    private Log log;

    private Mahasiswa mahasiswa;

    private MataKuliah mataKuliah;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1234567890");
        mahasiswa.setNama("Cahya Nur Hikari");
        mahasiswa.setEmail("cahya.hikari@ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081234567890");

        Calendar calendar = Calendar.getInstance();
        calendar.set(2019, 7, 7, 0, 0, 0);
        Date start = calendar.getTime();
        calendar.set(2019, 7, 7, 2, 0, 0);
        Date end = calendar.getTime();

        log = new Log();
        log.setIdLog(1);
        log.setStartTime(start);
        log.setEndTime(end);
        log.setDeskripsi("Hadeh");
        log.setMahasiswa(mahasiswa);

        mataKuliah = new MataKuliah("sdb45", "Struktur Data dan Bobaritma", "Ilkom");
    }

    @Test
    public void testControllerDaftarAsisten() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(mataKuliahService.getMataKuliah(mataKuliah.getKodeMatkul())).thenReturn(mataKuliah);
        when(mahasiswaService.setJadiAsdos(mahasiswa, mataKuliah)).thenReturn(mahasiswa);

        mvc.perform(post("/asisten-dosen/daftar/" + mahasiswa.getNpm() + "/" + mataKuliah.getKodeMatkul())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.npm").value(mahasiswa.getNpm()));
    }

    @Test
    public void testControllerDaftarAsistenIsAlreadyAsdos() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(mataKuliahService.getMataKuliah(mataKuliah.getKodeMatkul())).thenReturn(mataKuliah);
        mahasiswa.setMatkulDiAsdosi(mataKuliah);

        mvc.perform(post("/asisten-dosen/daftar/" + mahasiswa.getNpm() + "/" + mataKuliah.getKodeMatkul())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    public void testControllerGetListLog() throws Exception{
        List<Log> listLog = new ArrayList<>();
        listLog.add(log);

        when(logService.getListLog()).thenReturn(listLog);

        mvc.perform(get("/asisten-dosen/logList").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idLog").value("1"));
    }

    @Test
    public void testLogControllerGetLogById() throws Exception{
        when(logService.getLogByIdLog(log.getIdLog())).thenReturn(log);

        mvc.perform(get("/asisten-dosen/log/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.deskripsi").value("Hadeh"));
    }

    @Test
    public void testLogControllerGetLogByIdNotFound() throws Exception{
        mvc.perform(get("/asisten-dosen/log/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerUpdateLog() throws Exception {
        when(logService.getLogByIdLog(log.getIdLog())).thenReturn(log);

        Calendar calendar = Calendar.getInstance();
        calendar.set(2019, 7, 7, 0, 0, 0);
        Date start = calendar.getTime();
        calendar.set(2019, 7, 7, 2, 0, 0);
        Date end = calendar.getTime();

        Log tempLog = new Log(start, end, "Yahallo");

        String isiLog = "{\"startTime\":1234567890,\"endTime\":1234567890,\"deskripsi\":\"Yahallo\"}";

        when(logService.updateLog(anyInt(), any(Log.class))).thenReturn(tempLog);
        mvc.perform(put("/asisten-dosen/log/update/" + log.getIdLog())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(isiLog))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deskripsi").value("Yahallo"));
    }

    @Test
    public void testControllerUpdateLogNotFound() throws Exception {
        String isiLog = "{\"startTime\":1234567890,\"endTime\":1234567890,\"deskripsi\":\"Yahallo\"}";

        mvc.perform(put("/asisten-dosen/log/update/" + log.getIdLog())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(isiLog))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerDeleteLog() throws Exception {
        when(logService.getLogByIdLog(log.getIdLog())).thenReturn(log);

        mvc.perform(delete("/asisten-dosen/log/delete/" + log.getIdLog()))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testControllerDeleteLogNotFound() throws Exception {
        mvc.perform(delete("/asisten-dosen/log/delete/" + log.getIdLog()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerGetLogSummary() throws Exception {
        mahasiswa.setMatkulDiAsdosi(mataKuliah);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        List<LogSummary> res = new ArrayList<>();
        res.add(new LogSummary("7", 7, 7));
        when(logService.getLogSummary(mahasiswa)).thenReturn(res);

        mvc.perform(get("/asisten-dosen/summary/" + mahasiswa.getNpm()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].bulan").value("7"));
    }

    @Test
    public void testControllerGetLogSummaryMahasiswaNotFound() throws Exception {
        mvc.perform(get("/asisten-dosen/summary/" + mahasiswa.getNpm()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerGetLogSummaryMatkulDiAsdosiNotFound() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        mvc.perform(get("/asisten-dosen/summary/" + mahasiswa.getNpm()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerGetLogSummaryByMonth() throws Exception {
        mahasiswa.setMatkulDiAsdosi(mataKuliah);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        LogSummary res = new LogSummary("7", 7, 7);
        when(logService.getLogSummaryByMonth(mahasiswa, 7)).thenReturn(res);

        mvc.perform(get("/asisten-dosen/summaryMonth/" + mahasiswa.getNpm() + "/7"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.bulan").value("7"));
    }

    @Test
    public void testControllerGetLogSummaryByMonthMahasiswaNotFound() throws Exception {
        mvc.perform(get("/asisten-dosen/summaryMonth/" + mahasiswa.getNpm() + "/7"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerGetLogSummaryByMonthMatkulDiAsdosiNotFound() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        mvc.perform(get("/asisten-dosen/summaryMonth/" + mahasiswa.getNpm() + "/7"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerAddLog() throws Exception {
        mahasiswa.setMatkulDiAsdosi(mataKuliah);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(logService.createLog(any(Mahasiswa.class), any(Log.class))).thenReturn(log);

        String isiLog = "{\"startTime\":1234567890,\"endTime\":1234567890,\"deskripsi\":\"Yahallo\"}";

        mvc.perform(post("/asisten-dosen/log/create/" + mahasiswa.getNpm())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(isiLog))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deskripsi").value("Hadeh"));
    }

    @Test
    public void testControllerAddLogMahasiswaNotFound() throws Exception {
        String isiLog = "{\"startTime\":1234567890,\"endTime\":1234567890,\"deskripsi\":\"Yahallo\"}";

        mvc.perform(post("/asisten-dosen/log/create/" + mahasiswa.getNpm())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(isiLog))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerAddLogMatkulDiAsdosiNotFound() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        String isiLog = "{\"startTime\":1234567890,\"endTime\":1234567890,\"deskripsi\":\"Yahallo\"}";

        mvc.perform(post("/asisten-dosen/log/create/" + mahasiswa.getNpm())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(isiLog))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerGetMahasiswaLogList() throws Exception {
        mahasiswa.setMatkulDiAsdosi(mataKuliah);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        List<Log> res = new ArrayList<>();
        res.add(log);
        when(logService.getListLogByMahasiswa(mahasiswa)).thenReturn(res);

        mvc.perform(get("/asisten-dosen/log/" + mahasiswa.getNpm() + "/logList"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].idLog").value("1"));
    }

    @Test
    public void testControllerGetMahasiswaLogListMahasiswaNotFound() throws Exception {
        mvc.perform(get("/asisten-dosen/log/" + mahasiswa.getNpm() + "/logList"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerGetMahasiswaLogListMatkulDiAsdosiNotFound() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        mvc.perform(get("/asisten-dosen/log/" + mahasiswa.getNpm() + "/logList"))
                .andExpect(status().isNotFound());
    }
}
