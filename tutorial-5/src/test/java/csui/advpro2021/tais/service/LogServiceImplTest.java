package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @InjectMocks
    private LogServiceImpl logService;

    private Log log;
    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp() {
        Calendar calendar = Calendar.getInstance();

        calendar.set(2019, 7, 1, 0, 0, 0);
        Date startTime = calendar.getTime();

        calendar.set(2019, 7, 1, 2,0,0);
        Date endTime = calendar.getTime();

        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1234567890");
        mahasiswa.setNama("Cahya Nur Hikari");
        mahasiswa.setEmail("cahya.hikari@ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081234567890");

        log = new Log();
        log.setIdLog(1);
        log.setStartTime(startTime);
        log.setEndTime(endTime);
        log.setDeskripsi("Hadeh");
        log.setMahasiswa(mahasiswa);
    }

    @Test
    public void testServiceCreateLog() {
        lenient().when(logService.createLog(mahasiswa, log)).thenReturn(log);
    }

    @Test
    public void testServiceGetListLog() {
        List<Log> listLog = logRepository.findAll();
        lenient().when(logService.getListLog()).thenReturn(listLog);

        Iterable<Log> res = logService.getListLog();
        assertIterableEquals(listLog, res);

    }

    @Test
    public void testServiceGetLogByIdLog() {
        lenient().when(logService.getLogByIdLog(1)).thenReturn(log);
        Log res = logService.getLogByIdLog(log.getIdLog());
        assertEquals(log.getIdLog(), res.getIdLog());
    }

    @Test
    public void testServiceGetListLogByMahasiswa() {
        lenient().when(logService.getListLogByMahasiswa(mahasiswa)).thenReturn(mahasiswa.getLogList());
        List<Log> res = logService.getListLogByMahasiswa(mahasiswa);
        assertEquals(res, mahasiswa.getLogList());
    }

    @Test
    public void testServiceGetLogSummary() {
        List<Log> logList = new ArrayList<>();
        logList.add(log);
        lenient().when(logRepository.findListLogByMahasiswa(mahasiswa)).thenReturn(logList);

        List<LogSummary> expectedRes = new ArrayList<>();
        expectedRes.add(new LogSummary("August", 2, 700));

        Collection<LogSummary> res = logService.getLogSummary(mahasiswa);
        assertIterableEquals(expectedRes, res);
    }

    @Test
    public void testServiceGetLogSummaryByMonth() {
        List<Log> logList = new ArrayList<>();
        logList.add(log);
        lenient().when(logRepository.findListLogByMahasiswa(mahasiswa)).thenReturn(logList);

        LogSummary expectedRes = new LogSummary("August", 2, 700);
        LogSummary res = logService.getLogSummaryByMonth(mahasiswa, 7);
        assertEquals(expectedRes, res);
    }

    @Test
    public void testServiceGetLogSummaryByMonthFail() {
        List<Log> logList = new ArrayList<>();
        logList.add(log);
        lenient().when(logRepository.findListLogByMahasiswa(mahasiswa)).thenReturn(logList);

        LogSummary expectedRes = new LogSummary("August", 2, 700);
        LogSummary res = logService.getLogSummaryByMonth(mahasiswa, 1);
        assertNotEquals(expectedRes, res);
    }

    @Test
    public  void testServiceUpdateLog() {
        logService.createLog(mahasiswa, log);

        String temp = log.getDeskripsi();
        log.setDeskripsi("Yahallo");
        lenient().when(logService.updateLog(log.getIdLog(), log)).thenReturn(log);

        Log res = logService.updateLog(log.getIdLog(), log);
        assertEquals(res.getIdLog(), log.getIdLog());
        assertNotEquals(res.getDeskripsi(), temp);
    }

    @Test
    public void testServiceDeleteLogByIdLog() {
        logService.createLog(mahasiswa, log);
        logService.deleteLogByIdLog(log.getIdLog());
        lenient().when(logService.getLogByIdLog(log.getIdLog())).thenReturn(null);
        assertNull(logService.getLogByIdLog(log.getIdLog()));
    }
}
